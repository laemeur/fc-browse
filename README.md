# fc-browse

A font browser that runs in a console (xterm, etc) window.

![Animated GIF of fc-browse in action.](http://laemeur.sdf.org/img/fc-browse.gif)

Inspired by Siddarth Dushantha's [fontpreview](https://github.com/sdushantha/fontpreview), but mine is really stripped-down and hackish. Code quality aside, fc-browse does three things that I wanted:
- preview the font inside the terminal window
- allow changing/previewing font size on the fly
- provide a valid fontconfig pattern string for the selected font

Basically, I was looking for a better way to choose fonts for dmenu. There's no equivalent to xfontsel for fontconfig strings, so... here's a tool.

## REQUIREMENTS

You'll need ImageMagick, w3mimgdisplay, and fzf installed for this to work. If you're on a Debian-based system, you can make sure you've got those with:
~~~
$ sudo apt install imagemagick w3m-img fzf
~~~
Because this relies on w3mimgdisplay, it will not work in every terminal emulator. I use xterm, and it works in that. Last I checked it does *not* work in gnome-terminal. I really don't know about what other terminals it does or does not work in.

## TO INSTALL

All you need is the 'fc-browse' file. It's, like, 30 lines of bash. Stick it in your path somewhere, make it executable.

## TO USE
~~~
$ fc-browse [sample_text] [font_size]
~~~
The sample text must be quoted if it has spaces, of course. If no sample text is provided, the fragment "The quick brown fox..." will be used. Font size defaults to 30px.

Once you've selected a font in the browser, hit [Enter] and the program will print the fontconfig pattern and exit.